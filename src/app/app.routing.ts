import { HomeComponent, BodyComponent } from './components';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'todos',
        loadChildren: './modules/todos/todos.module#TodosModule',
      },
      {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
      }
    ]
  )
  ],
  exports: [
    RouterModule
  ]
})

export class AppRouting { }
