import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import {
  HeaderComponent,
  BodyComponent,
  RootComponent,
  HomeComponent,
} from './components';
import { AppRouting } from './app.routing';
import { SharedModule } from './modules/shared/shared.module';

@NgModule({
  declarations: [
    HeaderComponent,
    RootComponent,
    BodyComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    CommonModule,
    AppRouting,
    SharedModule,
  ],
  providers: [],
  bootstrap: [RootComponent]
})
export class AppModule { }
