import { VisibilityFilter } from './../constants';

export interface ITodo {
    index: number;
    text: string;
    isCompleted: boolean;
}
