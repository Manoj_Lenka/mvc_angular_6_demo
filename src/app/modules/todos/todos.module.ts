import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import {
    TodoListComponent,
    TodoInputComponent,
    TodoDisplayComponent,
    TodoComponent,
} from './components';
import { TodosRouting } from './todos.routing';

@NgModule({
    declarations: [
        TodoListComponent,
        TodoInputComponent,
        TodoDisplayComponent,
        TodoComponent,
    ],
    imports: [
        FormsModule,
        TodosRouting,
        CommonModule,
    ],
    providers: [],
})
export class TodosModule { }
