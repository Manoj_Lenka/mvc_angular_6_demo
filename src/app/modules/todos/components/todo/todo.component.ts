import { Component, OnInit, Input } from '@angular/core';
import { ITodo } from '../../../shared/models';
import { SharedService } from '../../../shared/services';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  @Input() todo: ITodo;

  constructor(
    private _sharedService: SharedService,
  ) { }

  ngOnInit() {
  }

  toggleSelected(index: number): void {
    this._sharedService.toggoleSelected(index);
  }

}
