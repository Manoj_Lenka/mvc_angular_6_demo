import { Component, OnInit, OnDestroy } from '@angular/core';
import { ITodo } from '../../../shared/models';
import { SharedService } from '../../../shared/services';
import { Subscription } from 'rxjs/internal/Subscription';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];

  todoList: ITodo[] = [];

  constructor(
    private _sharedService: SharedService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getTodoList();
  }

  getTodoList(): void {
    this.subscriptions.push(
      this._sharedService
      .getTodoList
      .subscribe((items: ITodo[]) => {
        this.todoList = items;
      })
    );
  }

  setText(text: string): void {
    this._sharedService.addTodoList(text);
  }

  navigateToHome(): void {
    this._router.navigate(['/'], {
      relativeTo: this._activatedRoute
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
  }

}
