import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from '../../modules/shared/services';
import { ITodo } from '../../modules/shared/models';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  private subscriptions: Subscription[] = [];

  count = 0;

  constructor(
    private _sharedService: SharedService,
  ) { }

  ngOnInit() {
    this.getTodoCount();
  }

  getTodoCount(): void {
    this.subscriptions.push(
      this._sharedService
        .getTodoList
        .subscribe((todoList: ITodo[]) => {
          this.count = todoList.filter(todo => !todo.isCompleted).length;
        })
    );
  }

}
